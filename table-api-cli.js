const { exec } = require('child_process');

function run_shell_command(command, cbOk) {
  const rPromise = new Promise((res, rej) => {
    exec(command, (err, stdout, stderr) => {
      if (err) {
        console.error('Failed to execute command:', command, 'stderr:', stderr);
        rej(err);
      } else {
        res(cbOk(stdout));
      }
    });
  })
  return rPromise;
}

function lookupStacks(branchName) {
  command = 'git ls-remote -h https://gitlab.com/mitigatech/training/deploy-util-stacks/deploy-util-stacks.git';
  return run_shell_command(command,
    (result) => {
      const branches = result.
        split(/[\n\t]/g).
        filter((element, index) => {
          return (index % 2 != 0);
        }).
        map((b) => b.slice(11));
      return branches.includes(branchName);
    }).catch(() => console.log('Failed to lookup'));
};
// USAGE:
// lookupStacks('main').then((res) => console.log(res))

async function saveStack(doc) {
  const stackExist = await lookupStacks(doc.stack);
  console.log('Stack exists?', stackExist);
  const opts = stackExist ? doc.stack : 'main';
  const command = `git clone -b ` + opts + ` git@gitlab.com:mitigatech/training/deploy-util-stacks/deploy-util-stacks.git && ` +
    `cd deploy-util-stacks && ` +
    (!stackExist ? `git checkout -b ` + doc.stack + ` && ` : '') +
    `echo '` + JSON.stringify(doc) + `' > stack.json && ` +
    `git add . && ` +
    `git commit -m save-commit && ` +
    `git push --set-upstream origin ` + doc.stack;
  run_shell_command(command, () => {
    console.log('Saved stack');
  }).
    catch(() => console.log('Failed to save stack, Possibly because of no changes made')).
    finally(() => run_shell_command(`rm -rf deploy-util-stacks`, () => {}));
  };
// USAGE:
// let deploymentOutputRecord = {
//   stack: 'stackName7',
//   deployment_output: 'deploymentOutput3',
//   company_id: 'customerId',
// };
// saveStack(deploymentOutputRecord);


async function deleteStack(stackName) {
  const stackExist = await lookupStacks(stackName);
  if (!stackExist) {
    console.log('No such stack exists', stackName);
  } else {
    console.log('Stack exists, deleting...');
    const command = `git clone -b ` + stackName + ` git@gitlab.com:mitigatech/training/deploy-util-stacks/deploy-util-stacks.git && ` +
      `cd deploy-util-stacks && ` +
      `git push origin --delete ` + stackName;
    await run_shell_command(command, (result) => {
      console.log(result);
    }).
      catch((err) => console.log('Failed to delete stack, err', err)).
      finally(() => run_shell_command(`rm -rf deploy-util-stacks`, (result) => {
        console.log(result)
      }));
  }
};
// USAGE:
// deleteStack('new-branch')