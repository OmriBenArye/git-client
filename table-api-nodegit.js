var Git = require("nodegit");
const path = require("path");
const fs = require("fs");

const repoUrl = 'https://gitlab.com/mitigatech/training/deploy-util-stacks/deploy-util-stacks.git'
const directoryName = './tmp'
const callback = {
    credentials: function () {
        return Git.Cred.userpassPlaintextNew('x-oauth-basic', 'XXXXXXXXX'); //PUT TOKEN HERE!
    },
    certificateCheck: function () {
        return 0;
    }
}

//lookup
async function lookupStacks(stackName) {
    const repo = await Git.Repository.init(directoryName, 1);
    try {
        await Git.Remote.delete(repo, 'origin');
    } catch (error) { }
    const remote = await Git.Remote.create(repo, 'origin', repoUrl);
    await remote.connect(Git.Enums.DIRECTION.FETCH, callback);
    const references = await remote.referenceList();
    const stacks = references.map((reference) => reference.name().slice(11));
    fs.rmSync(directoryName, { recursive: true, force: true });
    return stacks.includes(stackName);
}
// USAGE:
// lookupStacks('stackName1').then((res) => console.log(res))

//save
async function saveStack(record) {
    const stackName = record.stack;
    const fileName = 'stack.json'
    try {
        exists = await lookupStacks(stackName)
        if (exists) {
            console.log('stack', stackName, 'already exists');
            const cloneOptions = { fetchOpts: { callbacks: callback }, checkoutBranch: stackName }
            repo = await Git.Clone.clone(repoUrl, directoryName, cloneOptions)
        }
        else {
            //creating new branch and checkout
            const cloneOptions = { fetchOpts: { callbacks: callback } }
            repo = await Git.Clone.clone(repoUrl, directoryName, cloneOptions)
            const headCommit = await repo.getHeadCommit();
            branchRef = await repo.createBranch(stackName, headCommit, false);
            repo.checkoutBranch(branchRef);
        }
        //creating a stack file, add and commit
        await fs.promises.writeFile(path.join(repo.workdir(), fileName), JSON.stringify(record));
        const index = await repo.refreshIndex();
        await index.addByPath(fileName);
        await index.write();
        const oid = await index.writeTree();
        const author = Git.Signature.now("Deploy-Util", "deploy-util@mitiga.io");
        const committer = Git.Signature.now("Deploy-Util", "deploy-util@mitiga.io");
        const headCommit = await repo.getHeadCommit();
        await repo.createCommit("HEAD", author, committer, "message", oid, [headCommit]);

        //push
        remote = await repo.getRemote('origin');
        await remote.connect(Git.Enums.DIRECTION.PUSH, callback);
        // console.log('remote connected?', remote.connected());
        const refSpec = 'refs/heads/' + stackName + ':refs/heads/' + stackName;
        await remote.push([refSpec], { callbacks: callback })
    } catch (error) {
        console.log('failed to save stack. error:', error)
    }
    fs.rmSync(directoryName, { recursive: true, force: true });
}
// USAGE:
// let deploymentOutputRecord = {
//     stack: 'stackName5',
//     deployment_output: 'deploymentOutput1',
//     company_id: 'customerId',
// };
// saveStack(deploymentOutputRecord);

//delete
async function deleteStack(stackName) {
    const fileName = 'stack.json'
    const cloneOptions = { fetchOpts: { callbacks: callback, checkoutBranch: stackName } }
    exists = await lookupStacks(stackName)
    if (!exists) {
        console.log('stack', stackName, 'does not exists');
    }
    else {
        try {
            repo = await Git.Clone.clone(repoUrl, directoryName, cloneOptions);
            remote = await repo.getRemote('origin');
            const refSpec = ':refs/heads/' + stackName;
            await remote.push([refSpec], { callbacks: callback });
            console.log('deleted stack:', stackName);
        } catch (error) {
            console.log('failed to delete stack. error:', error);
        }

        fs.rmSync(directoryName, { recursive: true, force: true });
    }
}
// USAGE:
// await deleteStack('stackName2');
